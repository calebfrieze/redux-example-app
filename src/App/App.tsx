import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "./state/store";

import { BrowserRouter } from "react-router-dom";

const App: React.FC = () => (
  <Provider store={store}>
    <BrowserRouter />
  </Provider>
);

ReactDOM.render(<App />, document.getElementById("root"));
