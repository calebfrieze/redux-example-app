import * as React from "react";
import { Route, Switch } from "react-router-dom";

const ContentRouter: React.FC = () => {
  return (
    <Switch>
      <Route exact path="/" />
    </Switch>
  );
};

export default ContentRouter;
