export interface Loadable {
  isLoading: boolean;
  hasData: boolean;
  loadError: Error | null;
}

interface EmptyLoadable extends Loadable {
  hasData: false;
}

export type PopulatedLoadable<T> = T & Loadable & {
  hasData: true;
}

export const defaultLoadable: EmptyLoadable = Object.freeze({
  isLoading: false,
  hasData: false,
  loadError: null
});

export type LoadableData<T> = EmptyLoadable | PopulatedLoadable<T>;
