import { UsersServiceState, defaultUsersServiceState } from "../components/Users";

export interface ServicesState {
  usersService: UsersServiceState;
}

export interface AppState {
  services: ServicesState;
}

export const defaultAppState: AppState = {
  services: {
    usersService: defaultUsersServiceState
  }
};
