import { createStore } from "redux";
import { appStateReducer } from "./reducer";

export default createStore(appStateReducer);
