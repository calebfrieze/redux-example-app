import { combineReducers, Reducer, AnyAction } from "redux";
import { usersServiceReducer } from "../components/Users/store/reducer";
import { AppState, defaultAppState, ServicesState } from "./state";

const servicesReducer = combineReducers<ServicesState>({
  usersService: usersServiceReducer
});

export const appStateReducer: Reducer<AppState, AnyAction> = (state = defaultAppState, action): AppState => {
  return {
    ...state,
    services: servicesReducer(state.services, action)
  };
};
