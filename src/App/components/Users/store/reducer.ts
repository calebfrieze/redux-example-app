import { UsersServiceState, defaultUsersServiceState } from "..";
import { Reducer, AnyAction } from "redux";

export const usersServiceReducer: Reducer<UsersServiceState, AnyAction> = (state = defaultUsersServiceState, action): UsersServiceState => {
  return state;
};
