import { User } from "../types";
import { defaultLoadable, LoadableData } from "../../../state/types";

export interface UsersData {
  users: User[];
}

export type UsersServiceState = LoadableData<UsersData>;

export const defaultUsersServiceState: Readonly<UsersServiceState> = defaultLoadable;
