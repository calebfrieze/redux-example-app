interface UserLocation {
  countryCode: string;
  country: string;
  flagIconLink?: string;
}

interface UserLanguage {
  abbreviation: string;
  name: string;
}

export interface User {
  userUuid: string;
  email: string;
  firstName: string;
  lastName: string;
  publicProfile: boolean;
  status: string;
  created: string;
  updated: string;
  description?: string;
  profilePictureLink?: string;
  urlSlug?: string;
  age?: number;
  gender?: string;
  location?: UserLocation;
  language?: UserLanguage;
}
